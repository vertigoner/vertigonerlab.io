/**
 * Projects javascript file for noah-roberts.com
 * Inspired by Sol Lewitt's Wall Drawing 273
 *
 * Noah Roberts
*/


// ###############################
// SETUP & CONFIG
// ###############################

const PI = Math.PI;

// window properties
const MARGIN = 30;
const W = (window.innerWidth
|| document.documentElement.clientWidth
|| document.body.clientWidth) - MARGIN;
const H = (window.innerHeight
|| document.documentElement.clientHeight
|| document.body.clientHeight) - MARGIN;

const ICON = {
  "gitlab": `<svg class="project-gitlab-icon" aria-labelledby="simpleicons-gitlab-icon" role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>GitLab</title><path d="M23.955 13.587l-1.342-4.135-2.664-8.189c-.135-.423-.73-.423-.867 0L16.418 9.45H7.582L4.919 1.263C4.783.84 4.185.84 4.05 1.26L1.386 9.449.044 13.587c-.121.375.014.789.331 1.023L12 23.054l11.625-8.443c.318-.235.453-.647.33-1.024"/></svg>`,
  "github": `<svg class="project-github-icon" aria-labelledby="simpleicons-github-icon" role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>GitHub</title><path d="M12 .297c-6.63 0-12 5.373-12 12 0 5.303 3.438 9.8 8.205 11.385.6.113.82-.258.82-.577 0-.285-.01-1.04-.015-2.04-3.338.724-4.042-1.61-4.042-1.61C4.422 18.07 3.633 17.7 3.633 17.7c-1.087-.744.084-.729.084-.729 1.205.084 1.838 1.236 1.838 1.236 1.07 1.835 2.809 1.305 3.495.998.108-.776.417-1.305.76-1.605-2.665-.3-5.466-1.332-5.466-5.93 0-1.31.465-2.38 1.235-3.22-.135-.303-.54-1.523.105-3.176 0 0 1.005-.322 3.3 1.23.96-.267 1.98-.399 3-.405 1.02.006 2.04.138 3 .405 2.28-1.552 3.285-1.23 3.285-1.23.645 1.653.24 2.873.12 3.176.765.84 1.23 1.91 1.23 3.22 0 4.61-2.805 5.625-5.475 5.92.42.36.81 1.096.81 2.22 0 1.606-.015 2.896-.015 3.286 0 .315.21.69.825.57C20.565 22.092 24 17.592 24 12.297c0-6.627-5.373-12-12-12"/></svg>`,
  "video": `<svg class="project-video-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-3 17v-10l9 5.146-9 4.854z"/></svg>`,
  "private": `<svg class="project-private-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M21.658 3.786l-3.658 3.318v-1.104c0-3.313-2.687-6-6-6s-6 2.687-6 6v4h-3v10.707l-2 1.813 1.346 1.48 20.654-18.734-1.342-1.48zm-16.658 15.107v-6.893h7.625l-7.625 6.893zm11-9.975l-1.194 1.082h-6.806v-4c0-2.205 1.795-4 4-4s4 1.795 4 4v2.918zm5 1.082v14h-16.391l2.204-2h12.187v-10h-1.176l2.191-2h.985z"/></svg>`,
  "visualization": `<svg class="project-visualization-icon" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 485.8 485.8" style="enable-background:new 0 0 485.8 485.8;" xml:space="preserve"><g><g><path d="M346.3,440.6h2.3c-24-3-36.8-12.6-43.6-22.3H180.8c-6.8,9.7-19.6,19.3-43.6,22.3h1.5c-7.5,0-13.5,6.1-13.5,13.5c0,7.5,6.1,13.5,13.5,13.5h207.6c7.5,0,13.5-6.1,13.5-13.5C359.9,446.6,353.8,440.6,346.3,440.6z"/><path d="M485.8,307.8V53.5c0-19.5-15.8-35.3-35.3-35.3H35.3C15.8,18.2,0,34,0,53.5V314c0,0.1,0,0.3,0,0.4v47.2c0,0.2,0,0.4,0,0.6v2.6c0,0.2,0,0.4,0,0.6v0.7l0,0C0.6,382.7,14.3,396,31.1,396h143.3h14.3H297h8.8h148.8c16.8,0,30.4-13.3,31.1-29.9l0,0L485.8,307.8L485.8,307.8z M271.1,362.1c0,4.3-3.5,7.8-7.8,7.8h-40.9c-4.3,0-7.8-3.5-7.8-7.8v-5.4c0-4.3,3.5-7.8,7.8-7.8h40.9c4.3,0,7.8,3.5,7.8,7.8V362.1z M450.5,320.2H35.3c-3.4,0-6.2-2.8-6.2-6.2V53.5c0-3.4,2.8-6.2,6.2-6.2h415.1c3.4,0,6.2,2.8,6.2,6.2V314l0,0C456.6,317.4,453.9,320.2,450.5,320.2z"/><path d="M344.2,173.3h15.6l0.5-0.5c0-4.8-0.4-9.7-1.1-14.5L344.2,173.3z"/><path d="M308.4,84.7l-49.5,49.5v15.6l59-59C314.8,88.5,311.6,86.5,308.4,84.7z"/><path d="M286.4,75.6l-27.5,27.5v15.6l39-39C294.2,78,290.3,76.7,286.4,75.6z"/><path d="M273.6,72.9c-4.8-0.7-9.6-1.1-14.5-1.1l-0.2,0.2v15.6L273.6,72.9z"/><path d="M297.6,173.3l49.7-49.7c-1.8-3.2-3.8-6.4-6.1-9.5L282,173.3H297.6z"/><path d="M334.3,105.4c-1.2-1.3-2.4-2.6-3.7-3.9c-1.3-1.3-2.7-2.6-4.1-3.9l-67.6,67.7v7.9h7.6L334.3,105.4z"/><path d="M328.7,173.3l27.8-27.8c-1.1-3.9-2.5-7.7-4-11.5l-39.3,39.3H328.7z"/><path d="M241.2,89.3c-56.2,0-101.8,45.6-101.8,101.8S185,292.9,241.2,292.9S343,247.3,343,191.1H241.2V89.3z"/></g></g></svg>`
}

// line config
let baseLen = Math.max(W, H) / 3;
let numLines = 10;
let cluster = {
  left: new LineCluster(1, H / 2, 3 * PI / 2, PI / 2, baseLen, numLines, LINE_CLASS.YELLOW),
  right: new LineCluster(W - 1, H / 2, PI / 2, 3 * PI / 2, baseLen, numLines, LINE_CLASS.YELLOW)
};

// Vue components
Vue.component("project-item", {
  props: ["proj"],
  template: `<li class='project'>
              <div v-bind:class='proj.class'>
                <div v-html='proj.icon' class='tooltipicon'>
                </div>
                <a :href='proj.link'>
                  <div class='projtitle'>
                    <span>{{ proj.text }}</span>
                  </div>
                  <div class='projline'>
                    <svg xmlns='http://www.w3.org/2000/svg' version='1.1' width='${W / 2}' height='3'>
                      <line x1='0' y1='1' x2='75' y2='1'></line>
                    </svg>
                  </div>
                  <div class='projdesc'>
                    <span v-html='proj.desc'></span>
                  </div>
                </a>
              </div>
            </li>`
})





// ###############################
// MAIN CODE INTERFACING WITH HTML
// ###############################

window.onload = function() {
  let svg = document.getElementById("mainSVG");
  svg.setAttribute("width", W);
  svg.setAttribute("height", H);

  new Vue({
    el: "#proj-section",
    data: {
      projects: [
        { id: 5, text: "Mbed Air Hockey", link: "https://vertigoner.github.io/Mbed-Air-Hockey/", class: "tooltip github", icon: ICON["github"], desc: "Virtual web air hockey game using mbed controllers with speakers and IMU-based tilt controls. Controller Node.js servers run on the client computer(s) connected to the mbed controllers via serial." },
        { id: 4, text: "Candy Bubbles", link: "candy-bubbles/", class: "tooltip visualization", icon: ICON["visualization"], desc: "Data exploration tool made for the final project of my Intro to Info Visualization course with my good friend and partner, Sean! Visualized a candy dataset for users to aggregate people’s opinions, to compare candy popularity and to determine if certain demographics like age and gender suggest a person’s candy preferences." },
        { id: 0, text: "This site", link: "https://gitlab.com/vertigoner/vertigoner.gitlab.io", class: "tooltip gitlab", icon: ICON["gitlab"], desc: "This JavaScript-driven site makes significant use of the Snap.svg and Vue.js libraries. It was a great way to learn more about them by replicating Sol Lewitt's 1975 piece, <i>Wall Drawing 273</i>." },
        { id: 1, text: "WREKtranet3", link: "", class: "tooltip private", icon: ICON["private"], desc: "Currently working on a fully independant and functional intranet to facilitate WREK Atlanta's on-air shifts and admin tasks. Our database is a huge mess and previous solutions have been built on top of dated infrastructure. Repository is private (for now)!" },
        { id: 2, text: "WREK Scrob", link: "https://github.com/vertigoner/wrek-scrob", class: "tooltip github", icon: ICON["github"], desc: "Python app to 'scrobble' music played on WREK Atlanta to its last.fm profile. Scrapes current track from the web and logs it to last.fm using a custom python API interface." },
        { id: 3, text: "MBED Missile Command", link: "missile-command.mp4", class: "tooltip video", icon: ICON["video"], desc: "Implemented a functional version of the classic arcade game Missile Command in C++ using an MBED microcontroller and various external components (accelerometer, SD reader, speaker, push buttons, etc). " }
      ]
    }
  })

  init(Snap(svg));
  
  document.getElementById("back").addEventListener("click", function() {
    window.history.back();
  })
}

// build home page
function init(s) {

  // lines
  for (let key of Object.keys(cluster)) {
    let clusterGroup = s.g().attr({class: "cluster"});
    clusterGroup.node.id = key; // for some reason snap.svg doesn't let you set id easily

    clusterGroup.add(cluster[key].generate(s));
  }
}
