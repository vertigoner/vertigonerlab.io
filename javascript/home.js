/**
 * Homepage javascript file for noah-roberts.com
 * Inspired by Sol Lewitt's Wall Drawing 273
 *
 * Noah Roberts
*/

// ###############################
// ANIMATED TEXT CLASS
// ###############################

class AnimatedText {

  constructor(text, xi, yi, xf, yf, theta, fill) {
    this.text = text;
    this.xi = xi;
    this.yi = yi;
    this.xf = xf;
    this.yf = yf;
    this.theta = theta;
    this.fill = fill;
    this.x = xi;
    this.y = yi;
  }

  generate(s) { // takes snap object as parameter
    let text = genSnapText(s, this.text, this.x, this.y, this.theta);
    text.node.setAttribute("class", "nameLetter");
    text.node.style.fill = this.fill;

    this.snapText = text;

    return text;
  }

  menuAnimate(t, _callback=function() {return;}) { // time in ms
    this.snapText.animate({"x": this.xf.toString(), "y": this.yf.toString(),
      "transform": "r0, " + this.xf + ", " + this.yf }, t,
      mina.easein(0), _callback);
  }

  homeAnimate(t, _callback=function() {return;}) { // time in ms
    this.snapText.animate({"x": this.xi.toString(), "y": this.yi.toString(),
      "transform": "r" + -this.theta + ", " + this.xi + ", " + this.yi }, t,
      mina.easein(0), _callback);
  }
}

// create a text svg element
function genSnapText(s, text, x, y, theta) {
  let snapText = s.text(x, y, text);
  snapText.transform("rotate(" + -theta + ", " + x + ", " + y + ")");

  return snapText;
}





// ###############################
// SETUP & CONFIG
// ###############################

const PI = Math.PI;

// window properties
const MARGIN = 20;
const W = (window.innerWidth
|| document.documentElement.clientWidth
|| document.body.clientWidth) - MARGIN;
const H = (window.innerHeight
|| document.documentElement.clientHeight
|| document.body.clientHeight) - MARGIN;

// line config
let horzBaseLen = Math.max(W, H) / 3;
let vertBaseLen = Math.max(W, H) / 3;
let cornerBaseLen = Math.max(W, H) / 3;
let numLines = 10;
let cluster = {
  left: new LineCluster(1, H / 2, 3 * PI / 2, PI / 2, horzBaseLen, numLines, LINE_CLASS.RED),
  mid1: new LineCluster(W / 2, H / 2, 0, PI, horzBaseLen, numLines, LINE_CLASS.YELLOW),
  mid2: new LineCluster(W / 2, H / 2, PI, 2 * PI, horzBaseLen, numLines, LINE_CLASS.YELLOW),
  right: new LineCluster(W - 1, H / 2, PI / 2, 3 * PI / 2, horzBaseLen, numLines, LINE_CLASS.RED),
  top: new LineCluster(W / 2, 1, PI, 2 * PI, vertBaseLen, numLines, LINE_CLASS.RED),
  bot: new LineCluster(W / 2, H - 1, 0, PI, vertBaseLen, numLines, LINE_CLASS.RED),
  topLeft: new LineCluster(1, 1, 3 * PI / 2, 2 * PI, cornerBaseLen, numLines, LINE_CLASS.BLUE),
  topRight: new LineCluster(W - 1, 1, PI, 3 * PI / 2, cornerBaseLen, numLines, LINE_CLASS.BLUE),
  botLeft: new LineCluster(1, H - 1, 0, PI / 2, cornerBaseLen, numLines, LINE_CLASS.BLUE),
  botRight: new LineCluster(W - 1, H - 1, PI / 2, PI, cornerBaseLen, numLines, LINE_CLASS.BLUE)
};

// name config
const COLORS = ["#fc514b", "#ffdc00", "#504af9"];
let pickRandColor = function() {
  return COLORS[Math.floor(Math.random() * COLORS.length)];
}
let name = {
  n: new AnimatedText("N", W / 3, H / 3, W / 5, H / 10, Math.random() * 360, pickRandColor()),
  o: new AnimatedText("O", 2 * W / 3, H / 3, 2 * W / 5, H / 10, Math.random() * 360, pickRandColor()),
  a: new AnimatedText("A", W / 3, 2 * H / 3, 3 * W / 5, H / 10, Math.random() * 360, pickRandColor()),
  h: new AnimatedText("H", 2 * W / 3, 2 * H / 3, 4 * W / 5, H / 10, Math.random() * 360, pickRandColor())
};

// Vue components
Vue.component("menu-item", {
  props: ["menu"],
  template: `<li class='menuitem'>
              <a :href='menu.link'>
                <div class='menutext'>
                  <span>{{ menu.text }}</span>
                </div>
                <div class='menuline'>
                  <svg xmlns='http://www.w3.org/2000/svg' version='1.1' viewBox='0 0 20 3' width='${W / 2}' height='3'>
                    <line x1='0' y1='1' x2='20' y2='1' :stroke='menu.strokeColor'></line>
                  </svg>
                </div>
              </a>
            </li>`
})





// ###############################
// MAIN CODE INTERFACING WITH HTML
// ###############################

window.onload = function() {
  let svg = document.getElementById("mainSVG");
  svg.setAttribute("width", W);
  svg.setAttribute("height", H);

  new Vue({
    el: "#overlay",
    data: {
      menuItems: [
        { id: 0, text: "About me", link: "/about", strokeColor: COLORS[Math.floor(Math.random() * COLORS.length)] },
        { id: 1, text: "Projects", link: "/projects", strokeColor: COLORS[Math.floor(Math.random() * COLORS.length)] },
        { id: 2, text: "Resume", link: "/resume", strokeColor: COLORS[Math.floor(Math.random() * COLORS.length)] },
        { id: 3, text: "Original design by Sol Lewitt", link: "/img/sol_lewitt_wall_drawing_273.png", strokeColor: COLORS[Math.floor(Math.random() * COLORS.length)] }
      ]
    }
  })

  init(Snap(svg));

  document.getElementById("name").addEventListener("click", showMenu);
}

// build home page
function init(s) {

  // name
  let nameGroup = s.g();
  nameGroup.node.id = "name";
  for (let key of Object.keys(name)) {
    nameGroup.add(name[key].generate(s));
  }

  // lines
  for (let key of Object.keys(cluster)) {
    let clusterGroup = s.g().attr({class: "cluster"});
    clusterGroup.node.id = key; // for some reason snap.svg doesn't let you set id easily

    clusterGroup.add(cluster[key].generate(s));
  }
}

// animate name and show overlay
function showMenu() {
  for (let key of Object.keys(name)) {
    // letter animation
    name[key].menuAnimate(500, function() {
      // show overlay
      document.getElementById("overlay").style.display = "block";

      // toggle event listeners
      document.getElementById("name").removeEventListener("click", showMenu);
      window.addEventListener("click", hideMenu);
    });
  }
}

// restore homepage
function hideMenu(e) {
  // if click out of overlay div
  if (!document.getElementById("overlay").contains(e.target)){
    // hide overlay
    document.getElementById("overlay").style.display = "none";

    // letter animation
    for (let key of Object.keys(name)) {
      name[key].homeAnimate(650);
    }

    // toggle event listeners
    window.removeEventListener("click", hideMenu);
    document.getElementById("name").addEventListener("click", showMenu);
  }
}
