/**
 * 'About Me' javascript file for noah-roberts.com
 * Inspired by Sol Lewitt's Wall Drawing 273
 *
 * Noah Roberts
*/


// ###############################
// SETUP & CONFIG
// ###############################

const PI = Math.PI;

// window properties
const MARGIN = 20;
const W = (window.innerWidth
|| document.documentElement.clientWidth
|| document.body.clientWidth) - MARGIN;
const H = (window.innerHeight
|| document.documentElement.clientHeight
|| document.body.clientHeight) - MARGIN;

// line config
let baseLen = Math.max(W, H) / 3;
let numLines = 10;
let cluster = {
  left: new LineCluster(1, H / 2, 3 * PI / 2, PI / 2, baseLen, numLines, LINE_CLASS.YELLOW),
  right: new LineCluster(W - 1, H / 2, PI / 2, 3 * PI / 2, baseLen, numLines, LINE_CLASS.YELLOW)
};





// ###############################
// MAIN CODE INTERFACING WITH HTML
// ###############################

window.onload = function() {
  let svg = document.getElementById("mainSVG");
  svg.setAttribute("width", W);
  svg.setAttribute("height", H);

  init(Snap(svg));

  document.getElementById("back").addEventListener("click", function() {
    window.history.back();
  })
}

// build home page
function init(s) {

  // lines
  for (let key of Object.keys(cluster)) {
    let clusterGroup = s.g().attr({class: "cluster"});
    clusterGroup.node.id = key; // for some reason snap.svg doesn't let you set id easily

    clusterGroup.add(cluster[key].generate(s));
  }
}
